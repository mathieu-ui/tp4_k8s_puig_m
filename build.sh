#!/bin/bash

# Nom de l'application
NOM=tmp-serveur

# Version de l'application
VERSION=0.0.11

# Adresse du registre privé
REGISTRY="10.54.56.39:5000"

# Tag pour l'image Docker
TAG=$REGISTRY/$NOM:$VERSION

# Information de build
INFO="$VERSION build `date`"
echo $INFO

# Construire l'image Docker
docker build --rm -t $TAG --build-arg VERSION="$INFO" . --no-cache

# Pousser l'image Docker vers le registre privé
docker push $TAG

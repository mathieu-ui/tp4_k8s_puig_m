package main

import (
    "encoding/json"
    "net"
    "net/http"
    "os"
    "log"

    "github.com/go-chi/chi/v5"
    "github.com/go-chi/chi/v5/middleware"
)

type IPResponse struct {
    IP string `json:"ip"`
}

func getIP() (string, error) {
    interfaces, err := net.Interfaces()
    if err != nil {
        return "", err
    }

    for _, iface := range interfaces {
        addrs, err := iface.Addrs()
        if err != nil {
            return "", err
        }
        for _, addr := range addrs {
            var ip net.IP
            switch v := addr.(type) {
            case *net.IPNet:
                ip = v.IP
            case *net.IPAddr:
                ip = v.IP
            }
            if ip.IsGlobalUnicast() && !ip.IsLoopback() {
                return ip.String(), nil
            }
        }
    }
    return "", nil
}

func ipHandler(w http.ResponseWriter, r *http.Request) {
    ip, err := getIP()
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    response := IPResponse{IP: ip}
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(response)
}

func main() {
    r := chi.NewRouter()
    r.Use(middleware.Logger)
    r.Use(middleware.Recoverer)

    r.Get("/api/vo/ip", ipHandler)

    port := os.Getenv("PORT")
    if port == "" {
        port = "8080"
    }

    log.Printf("Starting server on :%s...", port)
    http.ListenAndServe(":"+port, r)
}

